const fs = require("fs");
const datas = "./datas/dataPaket.json";

//get all paket
exports.getAll = function (req, res) {
    fs.readFile(datas, "utf8", (err, data) => {
        if (err) {
            res.status(500).send({ "message": err });
            throw err;
        }
        var dataParse = JSON.parse(data);
        res.status(200).send(dataParse.koli_data);
    });
};

//get paket by id
exports.getPaketById = function (req, res) {
    fs.readFile(datas, "utf8", (err, data) => {
        if (err) {
            res.status(500).send({ "message": err });
            throw err;
        }
        var dataParse = JSON.parse(data);
        var dataFound = dataParse.koli_data.find(function (item) {
            return item.koli_id === req.params.id;
        });

        if (dataFound) {
            res.status(200).send(dataFound);
        }
        else {
            res.status(404).send({ "message": "data not found!!" });
        }
    });
};

//create new paket
exports.insertPaket = function (req, res) {
    fs.readFile(datas, "utf8", (err, data) => {
        if (err) {
            res.status(400).send({ "message": err });
            throw err;
        }

        var dataParse = JSON.parse(data);
        var dataFound = dataParse.koli_data.find(function (item) {
            return item.koli_id === req.body.koli_id;
        });

        if(dataFound){
            res.status(412).send({ "message": "data already exist!!" });
        }
        else {
            var dataKoli = dataParse.koli_data;
            var dataNew = {
                koli_length: req.body.koli_length
                , awb_url: req.body.awb_url
                , created_at: req.body.created_at
                , koli_chargeable_weight: req.body.koli_chargeable_weight
                , koli_width: req.body.koli_width
                , koli_surcharge: req.body.koli_surcharge
                , koli_height: req.body.koli_height
                , updated_at: req.body.updated_at
                , koli_description: req.body.koli_description
                , koli_formula_id: req.body.koli_formula_id
                , connote_id: req.body.connote_id
                , koli_volume: req.body.koli_volume
                , koli_weight: req.body.koli_weight
                , koli_id: req.body.koli_id
                , koli_custom_field: req.body.koli_custom_field
                , koli_code: req.body.koli_code
            };

            dataParse.koli_data = dataKoli.concat(dataNew);

            var dataSave = JSON.stringify(dataParse, null, 2);

            fs.writeFile(datas, dataSave, 'utf8', function (err) {
                if (err) {
                    res.status(500).send({ "message": err });
                    throw err;
                }
                res.status(200).send({ "message": "success insert new paket!!" });
            });
        }
    });
};

//update paket by id
exports.updatePaket = function (req, res) {
    fs.readFile(datas, "utf8", (err, data) => {
        if (err) {
            res.status(400).send({ "message": err });
            throw err;
        }
        var dataParse = JSON.parse(data);
        var dataFound = dataParse.koli_data.find(function (item) {
            return item.koli_id === req.params.id;
        });

        if (dataFound) {
            dataParse.koli_data.forEach((item) => {
                if (item.koli_id === req.params.id) {
                    item.koli_length = req.body.koli_length
                    item.awb_url = req.body.awb_url
                    item.created_at = req.body.created_at
                    item.koli_chargeable_weight = req.body.koli_chargeable_weight
                    item.koli_width = req.body.koli_width
                    item.koli_surcharge = req.body.koli_surcharge
                    item.koli_height = req.body.koli_height
                    item.updated_at = req.body.updated_at
                    item.koli_description = req.body.koli_description
                    item.koli_formula_id = req.body.koli_formula_id
                    item.connote_id = req.body.connote_id
                    item.koli_volume = req.body.koli_volume
                    item.koli_weight = req.body.koli_weight
                    item.koli_id = req.params.id
                    item.koli_custom_field = req.body.koli_custom_field
                    item.koli_code = req.body.koli_code
                }
            });

            var dataSave = JSON.stringify(dataParse, null, 2);

            fs.writeFile(datas, dataSave, 'utf8', function (err) {
                if (err) {
                    res.status(500).send({ "message": err });
                    throw err;
                }
                res.status(200).send({ "message": "success update paket id: " + req.params.id });
            });
        }
        else {
            res.status(404).send({ "message": "data not found!!" });
        }
    });
};

//delete paket bu id
exports.deletePaket = function (req, res) {
    fs.readFile(datas, "utf8", (err, data) => {
        if (err) {
            res.status(400).send({ "message": err });
            throw err;
        }
        var dataParse = JSON.parse(data);
        var dataKoli = dataParse.koli_data;
        var dataFound = dataParse.koli_data.find(function (item) {
            return item.koli_id === req.params.id;
        });

        if (dataFound) {
            dataParse.koli_data = dataKoli.filter(function (item) {
                return item.koli_id !== req.params.id;
            });

            var dataSave = JSON.stringify(dataParse, null, 2);

            fs.writeFile(datas, dataSave, 'utf8', function (err) {
                if (err) {
                    res.status(500).send({ "message": err });
                    throw err;
                }

                res.status(200).send({ "message": "success delete data with id: " + req.params.id });
            });
        }
        else {
            res.status(404).send({ "message": "data not found!!" });
        }
    });
};