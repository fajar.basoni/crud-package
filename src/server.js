const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const fs = require("fs");
var port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({origin: 'http://localhost:3000'}));

const routes = require("./routes/routes.js")(app, fs);

app.listen(port);
console.log("listening on port %s....", + port);