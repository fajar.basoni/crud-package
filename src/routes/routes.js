const controllerPaket = require("../controllers/controllerPaket");
const validation = require("../middlewares/middlewareValidation");

module.exports = app => {
    var router = require("express").Router();

    router.get("/", (req, res) => {
        res.send("welcome to the paket api-server");
    });
    router.get("/package", controllerPaket.getAll);
    router.get("/package/:id", controllerPaket.getPaketById);
    router.post("/package", validation.isValid, controllerPaket.insertPaket);
    router.put("/package/:id", validation.isValid, controllerPaket.updatePaket);
    router.patch("/package/:id", validation.isValid, controllerPaket.updatePaket);
    router.delete("/package/:id", controllerPaket.deletePaket);

    app.use(router);
}
    