const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');

chai.use(chaiHttp);
chai.should();

var id = '3600f10b-4144-4e58-a024-cc3178e7a700';
var dataRaw = {
    "koli_length": 0,
    "awb_url": "https://tracking.mile.app/label/AWB00100209082020.2",
    "created_at": "2020-07-15 11:11:13",
    "koli_chargeable_weight": 9,
    "koli_width": 0,
    "koli_surcharge": [],
    "koli_height": 0,
    "updated_at": "2020-07-15 11:11:13",
    "koli_description": "V WARP TEST INSERT",
    "koli_formula_id": null,
    "connote_id": "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
    "koli_volume": 0,
    "koli_weight": 9,
    "koli_id": "3600f10b-4144-4e58-a024-cc3178e7a700",
    "koli_custom_field": {
        "awb_sicepat": null,
        "harga_barang": null
    },
    "koli_code": "000000000000000000.0"
}

describe('Test API', function () {
    describe("POST /", () => {
        it("should post new koli data", (done) => {
            chai.request('http://localhost:3000')
                .post('/package')
                .send(dataRaw)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.to.be.an('object');
                    done();
                });
        });
    });
    describe("GET /", () => {
        it("should get all koli data", (done) => {
            chai.request('http://localhost:3000')
                .get('/package')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.to.be.an('object');
                    done();
                });
        });

        it("should get koli data by koli_id", (done) => {
            chai.request('http://localhost:3000')
                .get('/package/' + id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.to.be.an('object');
                    done();
                });
        });
    });
    describe("PUT /", () => {
        it("should update new koli data by koli_id", (done) => {
            chai.request('http://localhost:3000')
                .put('/package/' + id)
                .send(dataRaw)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.to.be.an('object');
                    done();
                });
        });
    });
    describe("PATCH /", () => {
        it("should update new koli data by koli_id", (done) => {
            chai.request('http://localhost:3000')
                .patch('/package/' + id)
                .send(dataRaw)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.to.be.an('object');
                    done();
                });
        });
    });
    describe("DELETE /", () => {
        it("should delete koli data by koli_id", (done) => {
            chai.request('http://localhost:3000')
                .delete('/package/' + id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.to.be.an('object');
                    done();
                });
        });
    });
});