const validator = require('../helpers/helperValidation');

const isValid = (req, res, next) => {
    const rules = {
          "koli_length": "required|integer"
        , "awb_url": "required|string"
        , "created_at": "required|string|date"
        , "koli_chargeable_weight": "required|integer"
        , "koli_width": "required|integer"
        , "koli_surcharge": "array"
        , "koli_height": "required|integer"
        , "updated_at": "required|string|date"
        , "koli_description": "required|string"
        , "connote_id": "required|string"
        , "koli_volume": "required|integer"
        , "koli_weight": "required|integer"
        , "koli_id": "required|string"
        , "koli_custom_field": {
            "awb_sicepat": "string",
            "harga_barang": "integer"
        }
        , "koli_code": "required|string"
    }
    validator(req.body, rules, {}, (err, status) => {
        if(!status){
            res.status(412)
            .send({
                "message" : "invalid data request!!",
                "data": err
            });
        }
        else {
            next();
        }
    });
}

module.exports = {
    isValid
}