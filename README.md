# crud-package

# Backend Crud With Json File Data Using Node.Js 
This is an API server for package/koli_data in some transaction data. In this project you are able to do all type of CRUD actions on package/koli_data. I am using dataPaket.json for storing the data.  You can run BDD tests or postman tests.

## How to run?
* Run `npm install` to install all node dependencies.
* Run `npm start` or `node server.js` in directory src/ to running the server.
* Run `npm test` in directory src/ to running the unit test.
* **You are done.** You can go `http://localhost:3000`.

## How to test?
* For the test you can read the documentation below as manual book.
* Read documentation for the API here : https://documenter.getpostman.com/view/11356963/T1DiHLc7?version=latest

**Thank You**